/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gc.metier;

import java.util.HashMap;



/**
 *
 * @author prive
 */
public class Club {
    private String nomClub;
    private int anneeCreationClub;
    private Responsable leResponsable;
    public HashMap<Adherent, Integer > lesAdherents = new HashMap();
    private Ville laVille;
    private Lieu leLieu;
    private Sport sportPratique = null;
    
    public Club(String nomClub, int anneeCreationClub, Responsable leResponsable, Ville laVille, Lieu leLieu) {
        this.nomClub = nomClub;
        this.anneeCreationClub = anneeCreationClub;
        this.leResponsable = leResponsable;
        this.laVille = laVille;
        this.leLieu = leLieu;
    }



    public String getNomClub() {
        return nomClub;
    }

    public int getAnneeCreationClub() {
        return anneeCreationClub;
    }

  
    public Responsable getLeResponsable() {
        return leResponsable;
    }


    public void ajouterAdherent(Adherent a){
        ///todo
    }
    public void enleverAdherent(Adherent a){
        ///todo
    }

    public Ville getLaVille() {
        return laVille;
    }


    public Lieu getLeLieu() {
        return leLieu;
    }

 
    
    
}
