/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gc.metier;

import java.util.ArrayList;

/**
 *
 * @author prive
 */
public class Gymnase extends Lieu{
    
    private boolean accesHandicape;
    private ArrayList<Sport> sportsCompatibles = new ArrayList<>();

    public Gymnase(boolean accesHandicape, String nomLieu, int longueur, int largeur, String surface, Ville laVille) {
        super(nomLieu, longueur, largeur, surface, laVille);
        this.accesHandicape = accesHandicape;
    }

    
    
    
    public void ajouterSport(Sport s){
        sportsCompatibles.add(s);
    }
    
}
