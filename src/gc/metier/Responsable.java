/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gc.metier;

/**
 *
 * @author prive
 */
public class Responsable extends Personne implements Contactable{
    private int telephone;
    private String email;

    public Responsable(int telephone, String email, String nom, String prenom) {
        super(nom, prenom);
        this.telephone = telephone;
        this.email = email;
    }

    
    
    @Override
    public void telephoner() {
        System.out.println("Appel du "+telephone);
    }

    @Override
    public void mailer() {
        System.out.println("Mail à "+email);
    }
    
    
}
