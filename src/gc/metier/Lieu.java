/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gc.metier;

/**
 *
 * @author prive
 */
public class Lieu {
    private String nomLieu;
    private int longueur;
    private int largeur;
    private String surface;
    private Ville laVille;

    public Lieu(String nomLieu, int longueur, int largeur, String surface, Ville laVille) {
        this.nomLieu = nomLieu;
        this.longueur = longueur;
        this.largeur = largeur;
        this.surface = surface;
        this.laVille = laVille;
    }
    
}
